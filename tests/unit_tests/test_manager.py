from datetime import datetime, timedelta
from pathlib import Path
from unittest import mock

import freezegun
from click.testing import CliRunner

from todotree.Config.Config import Config
from todotree.Task.Task import Task
from todotree.Managers.TaskManager import TaskManager


class TestTaskmanager:

    def test_empty(self):
        """Test that taskmanager can initialize without arguments."""
        TaskManager(Config())

    def test_import_tasks(self, monkeypatch):
        """Test that the import functionality works"""
        # Arrange.
        task_manager = TaskManager(Config())
        monkeypatch.setattr(Path, "open", mock.mock_open(read_data="First task\nSecond task\nThird task"))
        task_manager.config.paths.todo_file = Path("todo-simple.txt")
        test_data = [
            Task(1, "First task"),
            Task(2, "Second task"),
            Task(3, "Third task"),
        ]

        # Act.
        task_manager.import_tasks()

        # Assert.
        for i, task in enumerate(test_data):
            assert task_manager.task_list[i].i == test_data[i].i
            assert task_manager.task_list[i].task_string == test_data[i].task_string

    def test_filter_block(self):
        """Test that the block filter removes blocked tasks."""
        # Arrange.
        task_manager = TaskManager(Config())
        task_manager.task_list = [
            Task(1, "Task which is blocked by:b"),
            Task(2, "Task which blocks bl:b")
        ]

        # Act.
        task_manager.filter_block()

        # Assert.
        assert len(task_manager.task_list) == 1
        assert task_manager.task_list[0].i == 2

    def test_high_task(self):
        """Test that the padding of the list is correct, even in the case with tasks which have a lot hidden."""
        task_manager = TaskManager(Config())
        task_manager.task_list = [
            Task(1, "First"),
            Task(100000, "Second")
        ]

        # Act.
        result = str(task_manager)
        result_project = task_manager.print_tree("projects")
        result_context = task_manager.print_tree("contexts")

        # assert.
        assert "00001" in result
        assert "00001" in result_project
        assert "00001" in result_context

    def test_filter_tdate(self):
        """Test that the t:date filter removes tasks that are in the future"""
        # Arrange.
        task_manager = TaskManager(Config())
        task_manager.task_list = [
            Task(1, "enabled task t:1991-01-01"),
            Task(2, "disabled task t:2052-01-01"),
        ]

        # Act.
        task_manager.filter_t_date()

        # Assert.
        assert task_manager.task_list == [task_manager.task_list[0]]

    def test_import_projecttree_folder(self):
        """Test that the import projecttree functionality works"""
        # Arrange.
        with CliRunner().isolated_filesystem():
            (Path("project_test") / "project1").mkdir(parents=True, exist_ok=True)
            (Path("project_test") / "project2").mkdir(parents=True, exist_ok=True)
            (Path("project_test") / "project3").mkdir(parents=True, exist_ok=True)

            # Act.
            task_manager = TaskManager(Config())
            task_manager.config.paths.project_tree_folder = Path("project_test")

            # Act.
            task_manager._import_project_folder()

            # Assert.
            print(task_manager.config.paths.project_tree_folder)
            assert len(task_manager.task_list) == 3
            projects = [y for x in task_manager.task_list for y in x.projects]
            assert "project1" in projects
            assert "project2" in projects
            assert "project3" in projects

    def test_filter_filter(self):
        """Test that providing a filter works."""
        # Arrange.
        task_manager = TaskManager(Config())
        task_manager.task_list = [
            Task(1, "First task"),
            Task(2, "Second task"),
            Task(3, "Third task"),
            Task(4, "Second second task")
        ]

        # Act.
        task_manager.filter("Second")

        # Assert.
        assert len(task_manager.task_list) == 2
        assert task_manager.task_list[0].i == 2
        assert task_manager.task_list[1].i == 4

    @freezegun.freeze_time("2023-07-08")
    def test_tree_due_date(self, monkeypatch):
        """Test that Task manager can print a tree by due date."""
        # Arrange.
        task_manager = TaskManager(Config())
        task_manager.task_list = [
            Task(1, "Long Overdue task due:" + str((datetime.today() - timedelta(days=100)).date())),
            Task(2, "Recent Overdue task due:" + str((datetime.today() - timedelta(days=100)).date())),
            Task(3, "Task due today due:" + str((datetime.today().date()))),
            Task(4, "Task due tomorrow due:" + str((datetime.today() + timedelta(days=1)).date())),
            Task(5, "Task due this week due:" + str((datetime.today() + timedelta(days=5)).date())),
            Task(6, "Task due in the future due:" + str((datetime.today() + timedelta(days=100)).date())),
            Task(1, "Task without due")
        ]
        # Act.
        result = task_manager.print_tree("due")

        # Assert.
        assert result == """Due Dates
 +--Overdue
 |   +--1 Long Overdue task due:2023-03-30
 |   *--2 Recent Overdue task due:2023-03-30
 +--Due today
 |   *--3 Task due today due:2023-07-08
 +--Due tomorrow
 |   *--4 Task due tomorrow due:2023-07-09
 +--Due in the next 7 days
 |   *--5 Task due this week due:2023-07-13
 +--Due date further than the next 7 days
 |   *--6 Task due in the future due:2023-10-16
 *--No due date
     *--1 Task without due
"""

    def test_tree_project_date(self):
        """Test that task manager can print a tree by projects."""
        # Arrange.
        task_manager = TaskManager(Config())
        task_manager.task_list = [
            Task(1, "+First task"),
            Task(2, "+Second task"),
            Task(3, "+Third task"),
            Task(4, "+Second second task")
        ]

        # Act.
        result = task_manager.print_tree("projects")

        # Assert.
        assert result == """Projects
 +--First
 |   *--1 task
 +--Second
 |   +--2 task
 |   *--4 second task
 *--Third
     *--3 task
"""

    def test_tree_context_date(self):
        """Test that task manager can print a tree by context."""
        # Arrange.
        task_manager = TaskManager(Config())
        task_manager.task_list = [
            Task(1, "@First task"),
            Task(2, "@Second task"),
            Task(3, "@Third task"),
            Task(4, "@Second second task")
        ]

        # Act.
        result = task_manager.print_tree("contexts")

        # Assert.
        assert result == """Contexts
 +--First
 |   *--1 task
 +--Second
 |   +--2 task
 |   *--4 second task
 *--Third
     *--3 task
"""
