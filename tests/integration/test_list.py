from pathlib import Path

import freezegun
from click.testing import CliRunner

from tests.helper import setup_files
from todotree.Errors.TodoFileNotFoundError import TodoFileNotFoundError
from todotree.Main import root
from todotree.Managers.AbstractManager import AbstractManager


class TestList:
    """Test the list command."""
    def test_list_simple(self, monkeypatch):
        """Test list command."""
        # Arrange.
        runner = CliRunner()
        todo_content = "Task 1 \n Task 2\n"
        done_content = "x 2020-01-01 Done task\n"
        with runner.isolated_filesystem():
            todo_file, done_file, *_ = setup_files(monkeypatch, todotxt_content=todo_content,
                                                   donetxt_content=done_content)
            # Act.
            result = runner.invoke(root, 'list')

            # Assert.
            assert result.exit_code == 0
            assert todo_file.read_text() == todo_content
            assert done_file.read_text() == done_content


    def test_list(self, monkeypatch):
        """Test the list command. Also tests the --todo-file option."""
        # Arrange.
        runner = CliRunner()

        # Act.
        with runner.isolated_filesystem():
            setup_files(monkeypatch)
            Path("todo-simple.txt").write_text("First task\nSecond task\nThird task\n")
            result = runner.invoke(root, "--todo-file ./todo-simple.txt list")

        # Assert.
        print(result.output)
        assert result.exit_code == 0
        assert result.output == """ * Todos
1 First task
2 Second task
3 Third task

"""

    @freezegun.freeze_time("2023-08-02")
    def test_list_example(self, monkeypatch):
        """Test the list command with the example."""
        # Arrange.
        runner = CliRunner()
        example_todo = Path("../todotree/examples/todo.txt").read_text()

        # Act.
        with runner.isolated_filesystem():
            setup_files(monkeypatch, todotxt_content=example_todo)
            result = runner.invoke(root, "list")

        # Assert.
        if result.exit_code != 0:
            print(result.output)
        assert result.exit_code == 0
        assert result.output == """ * Todos
01 (A) This is an example todo.txt file
07 (B) Learn about all todotree's actions
08 (C) Migrate all todo's to todotree
02 (D) Example once in a lifetime due date due:2021-02-20
03 (D) Example once in a year due date due:2023-02-20
04 (D) Example once in a month due date due:2023-08-02
05 (D) Rewrite regex such that all three above can be done with just "due" +todotree
06 (E) A project is denoted with a plus, +example
09 (E) A context is denoted with an ampersand: @hometest
10 (F) A task which is not shown until Christmas 2020
11 (G) A task which blocks another task bl:a
15 (T) Test: block and tdate. This task should be hidden because block +thind
22 (T) Test: because this task. +nothidden
25 (T) Test: because this task. @nothidden

"""

    def test_list_empty(self, monkeypatch):
        """Test the behaviour when todo.txt is empty."""
        # Arrange.
        runner = CliRunner()
        done_content = "x 2020-01-01 Done task\n"
        with runner.isolated_filesystem():
            _, todo_file, *_ = setup_files(monkeypatch, donetxt_content=done_content)

            print("ping")
            # Create some projects
            f: Path
            f = todo_file.parent
            (f / "todo.txt").touch()
            print("ping")
            result = runner.invoke(root, "list")
        print(result.output)
        assert result.exit_code == 0


    def test_list_only_projects(self, monkeypatch):
        """Test the behaviour when todo.txt is empty and project_folder is enabled."""
        # Arrange.
        runner = CliRunner()
        done_content = "x 2020-01-01 Done task\n"
        with runner.isolated_filesystem():
            config_content = f"""
main:
    enable_project_folder: true
paths:
    project_folder: {(Path.cwd() / "project").absolute()}
            """
            setup_files(monkeypatch, donetxt_content=done_content, configyaml_content=config_content)

            # Create some projects
            cwd = Path.cwd()
            (cwd / "project" / "projectA").mkdir(parents=True, exist_ok=True)
            (cwd / "project" / "projectB").mkdir(parents=True, exist_ok=True)
            (cwd / "project" / "projectC").mkdir(parents=True, exist_ok=True)
            (cwd / "xdg" / "data" / "todotree" /"todo.txt").touch()
            result = runner.invoke(root, "list")
        print(result.output)
        assert result.exit_code == 0
        assert "+projectA" in result.output
        assert "+projectB" in result.output
        assert "+projectC" in result.output



    def test_no_file_found_list(self, monkeypatch):
        """
        Test that an error is returned when files are not found using `list`.
        """
        # Arrange.
        def mock_open(*args, **kwargs):
            raise TodoFileNotFoundError("Not Found")
        monkeypatch.setattr(AbstractManager, "import_tasks", mock_open)
        runner_todo = CliRunner()
        with runner_todo.isolated_filesystem():
            setup_files(monkeypatch)
            # Act.
            result_todo = runner_todo.invoke(root, "list")

        # Assert
        assert result_todo.exit_code == 1
        assert result_todo.output[0:58] == "!!!The todo.txt could not be found.\n!!!It searched at the "
