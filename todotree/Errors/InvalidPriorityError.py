from todotree.Errors.TodotreeError import TodotreeError


class InvalidPriorityError(TodotreeError):
    pass
