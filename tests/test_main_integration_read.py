from pathlib import Path

import freezegun
from click.testing import CliRunner

from tests.helper import setup_files
from todotree.Main import root


class TestMainIntegrationRead:
    """Test endpoints which do not edit, only read."""

    def test_empty(self):
        """Test the default command (which is help)"""
        runner = CliRunner()
        result = runner.invoke(root)
        assert result.exit_code == 0
        assert result.output[:7] == "Usage: "

    def test_help(self):
        """Test that explicitly calling help works."""
        runner = CliRunner()
        result = runner.invoke(root, "help")

        assert result.exit_code == 0
        assert "Commands:" in result.output
        assert "Options:" in result.output
        assert "Usage:" in result.output

    def test_config(self, monkeypatch):
        """Test that using a different config file works."""
        # Arrange.
        runner = CliRunner()
        with runner.isolated_filesystem():
            setup_files(monkeypatch)
            Path("test_config.yaml").write_text("""
main:
    # Folder where your projects are
    projecttree_folder: ./project_test
    # Folder where your wishlist files are
    wishlist_folder: ./wishlist_test
    
""")

            # Act.
            result = runner.invoke(root, "--config-file ./test_config.yaml --help")

        # Assert.
        assert result.exit_code == 0
        assert result.output[:7] == "Usage: "

    def test_print_raw(self, monkeypatch):
        # Arrange.
        runner = CliRunner()
        with runner.isolated_filesystem():
            setup_files(monkeypatch, todotxt_content="todo1\ntodo2\ntodo3")

            # Act.
            result = runner.invoke(root, "print_raw")

        # Assert.
        print(result.output)
        assert result.exit_code == 0
        assert result.output == "todo1\ntodo2\ntodo3\n"

    def test_filter(self, monkeypatch):
        """Test the filter command"""
        # Arrange.
        runner = CliRunner()

        # Act.
        with runner.isolated_filesystem():
            setup_files(monkeypatch, todotxt_content="First task\nSecond task\nThird task\n")
            result = runner.invoke(root, "filter Second")

        assert result.exit_code == 0
        assert result.stdout == """ * Todos for term 'Second'
2 Second task

"""

    def test_list_done(self, monkeypatch):
        """Test the list_done command. Also tests the --done-file option"""
        # Arrange.
        runner = CliRunner()

        # Act.
        with runner.isolated_filesystem():
            setup_files(monkeypatch,
                        donetxt_content="x 2020-01-01 First task\nx 2021-01-01 Second task\nx 2022-01-01 Third task\n")
            result = runner.invoke(root, "list_done")

        # Assert.
        assert result.exit_code == 0
        assert result.output == """ * 3 x 2022-01-01 Third task
2 x 2021-01-01 Second task
1 x 2020-01-01 First task

"""


