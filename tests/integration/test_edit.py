import click
from click.testing import CliRunner

from tests.helper import setup_files
from todotree.Main import root


class TestEdit:
    """Integration tests for the edit command."""

    def test_edit(self, monkeypatch):
        """Test the edit command with default argument."""

        # Arrange.
        def mock_edit(*args, **kwargs):
            edited_file = kwargs["filename"]
            with open(edited_file, "a") as f:
                f.write("This is typed in.")

        monkeypatch.setattr(click, "edit", mock_edit)
        runner = CliRunner()
        todo_content = "Task which is unfinished\nTask which is marked\nTask which is unfinished\n"
        done_content = "x 2020-01-01 Done task\nx 2023-07-10 Task which is marked\n"
        with runner.isolated_filesystem():
            todo_file, done_file, *_ = setup_files(monkeypatch, todotxt_content=todo_content,
                                                   donetxt_content=done_content)
            # Act.
            result = runner.invoke(root, ' edit')

            # Assert.
            assert result.exit_code == 0
            assert "This is typed in." in todo_file.read_text()
            assert done_content in done_file.read_text()

    def test_edit_todo(self, monkeypatch):
        """Test the edit command with todo as the argument."""

        # Arrange.
        def mock_edit(*args, **kwargs):
            edited_file = kwargs["filename"]
            with open(edited_file, "a") as f:
                f.write("This is typed in.")

        monkeypatch.setattr(click, "edit", mock_edit)
        runner = CliRunner()
        todo_content = "Task which is unfinished\nTask which is marked\nTask which is unfinished\n"
        done_content = "x 2020-01-01 Done task\nx 2023-07-10 Task which is marked\n"
        with runner.isolated_filesystem():
            todo_file, done_file, *_ = setup_files(monkeypatch, todotxt_content=todo_content,
                                                   donetxt_content=done_content)
            # Act.
            result = runner.invoke(root, ' edit todo')

            # Assert.
            assert result.exit_code == 0
            assert "This is typed in." in todo_file.read_text()
            assert done_content in done_file.read_text()

    def test_edit_recur(self, monkeypatch):
        """Test the edit command with recur as the argument."""

        # Arrange.
        def mock_edit(*args, **kwargs):
            edited_file = kwargs["filename"]
            with open(edited_file, "a") as f:
                f.write("This is typed in.")

        monkeypatch.setattr(click, "edit", mock_edit)
        runner = CliRunner()
        todo_content = "Task which is unfinished\nTask which is marked\nTask which is unfinished\n"
        done_content = "x 2020-01-01 Done task\nx 2023-07-10 Task which is marked\n"
        with runner.isolated_filesystem():
            todo_file, done_file, config, stale = setup_files(monkeypatch, todotxt_content=todo_content,
                                                   donetxt_content=done_content, recur_content="Some Content")
            # Act.
            result = runner.invoke(root, ' edit recur')

            # Assert.
            assert result.exit_code == 0
            recur_file = todo_file.parent / "recur.txt"
            assert "This is typed in." in recur_file.read_text()
            assert done_content in done_file.read_text()
            assert todo_content in todo_file.read_text()

    def test_edit_done(self, monkeypatch):
        """Test the edit command with done as the argument."""

        # Arrange.
        def mock_edit(*args, **kwargs):
            edited_file = kwargs["filename"]
            with open(edited_file, "a") as f:
                f.write("This is typed in.")

        monkeypatch.setattr(click, "edit", mock_edit)
        runner = CliRunner()
        todo_content = "Task which is unfinished\nTask which is marked\nTask which is unfinished\n"
        done_content = "x 2020-01-01 Done task\nx 2023-07-10 Task which is marked\n"
        with runner.isolated_filesystem():
            todo_file, done_file, *_ = setup_files(monkeypatch, todotxt_content=todo_content,
                                                   donetxt_content=done_content)
            # Act.
            result = runner.invoke(root, ' edit done')

            # Assert.
            assert result.exit_code == 0
            assert "This is typed in." in done_file.read_text()
            assert todo_content in todo_file.read_text()

    def test_edit_stale(self, monkeypatch):
        """Test the edit command with the stale file."""

        # Arrange.
        def mock_edit(*args, **kwargs):
            edited_file = kwargs["filename"]
            with open(edited_file, "a") as f:
                f.write("This is typed in.")

        monkeypatch.setattr(click, "edit", mock_edit)
        runner = CliRunner()
        todo_content = "Task which is unfinished\nTask which is marked\nTask which is unfinished\n"
        done_content = "x 2020-01-01 Done task\nx 2023-07-10 Task which is marked\n"
        with runner.isolated_filesystem():
            todo_file, done_file, *_ = setup_files(monkeypatch, todotxt_content=todo_content,
                                                   donetxt_content=done_content)
            # Act.
            result = runner.invoke(root, ' edit stale')

            # Assert.
            stale_file = todo_file.parent / "stale.txt"
            assert result.exit_code == 0
            assert "This is typed in." in stale_file.read_text()
            assert done_content in done_file.read_text()
            assert todo_content in todo_file.read_text()

    def test_edit_garbage(self, monkeypatch):
        """Test the edit command with an incorrect command."""

        # Arrange.
        def mock_edit(*args, **kwargs):
            edited_file = kwargs["filename"]
            with open(edited_file, "a") as f:
                f.write("This is typed in.")

        monkeypatch.setattr(click, "edit", mock_edit)
        runner = CliRunner()
        todo_content = "Task which is unfinished\nTask which is marked\nTask which is unfinished\n"
        done_content = "x 2020-01-01 Done task\nx 2023-07-10 Task which is marked\n"
        with runner.isolated_filesystem():
            todo_file, done_file, *_ = setup_files(monkeypatch, todotxt_content=todo_content,
                                                   donetxt_content=done_content)
            # Act.
            result = runner.invoke(root, ' edit garbage')

            # Assert.
            stale_file = todo_file.parent / "stale.txt"
            assert result.exit_code == 2
            assert done_content in done_file.read_text()
            assert todo_content in todo_file.read_text()
