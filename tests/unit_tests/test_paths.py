from pathlib import Path

import xdg_base_dirs

from todotree.Config.Paths import Paths


class TestPaths:

    def test_defaults(self):
        """Test that the defaults are active if none of the paths are set."""
        p = Paths()

        assert p.todo_folder == xdg_base_dirs.xdg_data_home() / "todotree"
        assert p.todo_file == p.todo_folder / "todo.txt"
        assert p.done_file == p.todo_folder / "done.txt"
        assert p.project_tree_folder == p.todo_folder / "projects"
        assert p.addons_folder == p.todo_folder / "addons"

    def test_defaults_with_folder(self):
        """Test that the defaults are changed if the folder variable is changed."""
        p = Paths()
        testpath = Path("other")
        p.todo_folder = testpath

        assert p.todo_folder == testpath
        assert p.todo_file == testpath / "todo.txt"
        assert p.done_file == testpath / "done.txt"
        assert p.project_tree_folder == testpath / "projects"
        assert p.addons_folder == testpath / "addons"

    def test_custom(self):
        """Test that all paths can be changed."""
        p = Paths()
        p.todo_folder = Path("todo_folder")
        p.todo_file = Path("todo_file")
        p.done_file = Path("done_file")
        p.project_tree_folder = Path("pf")
        p.addons_folder = Path("addons")

        assert p.todo_folder == Path("todo_folder")
        assert p.todo_file == Path("todo_file")
        assert p.done_file == Path("done_file")
        assert p.project_tree_folder == Path("pf")
        assert p.addons_folder == Path("addons")

    def test_from_dict_default(self):
        """Test that defaults are still active if an empty dict is loaded."""
        p = Paths()
        p.read_from_dict({})

        # Assert.
        assert p.todo_folder == xdg_base_dirs.xdg_data_home() / "todotree"
        assert p.todo_file == p.todo_folder / "todo.txt"
        assert p.done_file == p.todo_folder / "done.txt"
        assert p.project_tree_folder == p.todo_folder / "projects"
        assert p.addons_folder == p.todo_folder / "addons"

    def test_from_dict_custom(self):
        """Test that all paths can be changed by reading from a dict."""
        p = Paths()
        p.read_from_dict({
            'folder': "todo_folder",
            'todo_file': 'todo_file',
            'done_file': 'done_file',
            'project_folder': 'pf',
            'addons_folder': 'addons'
        })
        assert p.todo_folder == Path("todo_folder")
        assert p.todo_file == Path("todo_file")
        assert p.done_file == Path("done_file")
        assert p.project_tree_folder == Path("pf")
        assert p.addons_folder == Path("addons")
