from pathlib import Path

import freezegun
from click.testing import CliRunner

from tests.helper import setup_files
from todotree.Errors.TodoFileNotFoundError import TodoFileNotFoundError
from todotree.Main import root
from todotree.Managers.AbstractManager import AbstractManager


class TestContext:
    """Integration tests for context command."""

    def test_context_tree(self, monkeypatch):
        """Test the context tree command."""
        # Arrange.
        runner = CliRunner()

        # Act.
        with runner.isolated_filesystem():
            setup_files(monkeypatch, todotxt_content="First task\nSecond task\nThird task\n")
            result = runner.invoke(root, "context")

        # Assert.
        assert result.exit_code == 0
        assert result.output == """Contexts
 *--No Context
     +--1 First task
     +--2 Second task
     *--3 Third task

"""


    def test_no_file_found_context(self, monkeypatch):
        """
        Test that an error is returned when files are not found using `context`.
        """
        # Arrange.
        def mock_open(*args, **kwargs):
            raise TodoFileNotFoundError("Not Found")
        monkeypatch.setattr(AbstractManager, "import_tasks", mock_open)
        runner_todo = CliRunner()
        with runner_todo.isolated_filesystem():
            setup_files(monkeypatch)
            # Act.
            result_todo = runner_todo.invoke(root, "context")

        # Assert
        assert result_todo.exit_code == 1
        assert result_todo.output[0:58] == "!!!The todo.txt could not be found.\n!!!It searched at the "
