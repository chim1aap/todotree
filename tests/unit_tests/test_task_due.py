from datetime import datetime

from freezegun import freeze_time

from todotree.Errors.TaskParseError import TaskParseError
from todotree.Task.Task import Task


class TestTaskDue:
    """Test due date functionality"""
    year = datetime.today().year
    month = datetime.today().month

    def test_due_date_basic(self):
        """Happy flow of a due date."""
        t = Task(1, "basic due date due:2020-12-29")
        assert t.i == 1
        assert t.blocked == []
        assert t.blocks == []
        assert t.contexts == []
        assert t.due_date == datetime(2020, 12, 29).date()
        assert t.priority == 684
        assert t.projects == []
        assert t.t_date is None
        assert t.task_string == "basic due date due:2020-12-29"
        assert t.other_string == "basic due date"

    def test_due_date_multiple(self):
        """Happy flow of multiple due dates."""
        t = Task(1, "basic due date due:2020-12-29 due:2022-12-30")
        assert t.i == 1
        assert t.blocked == []
        assert t.blocks == []
        assert t.contexts == []
        assert t.due_date == datetime(2020, 12, 29).date()
        assert t.priority == 684
        assert t.projects == []
        assert t.t_date is None
        assert t.task_string == "basic due date due:2020-12-29 due:2022-12-30"
        assert t.other_string == "basic due date"

    def test_due_date_invalid(self):
        """Flow of an invalid due date. 2021 is not a leap year."""
        try:
            Task(1, "basic due date due:2021-02-29")
        except TaskParseError:
            pass

    def test_duy_date_basic(self):
        """Happy flow of a due date."""
        t = Task(1, "basic due date duy:12-29")
        assert t.i == 1
        assert t.blocked == []
        assert t.blocks == []
        assert t.contexts == []
        assert t.due_date == datetime(self.year, 12, 29).date()
        assert t.priority == 684
        assert t.projects == []
        assert t.t_date is None
        assert t.task_string == "basic due date duy:12-29"
        assert t.other_string == "basic due date"

    @freeze_time("2020-11-20")
    def test_duy_date_start(self):
        """Happy flow of a due date at the start of the year."""
        t = Task(1, "basic due date duy:01-02")
        assert t.i == 1
        assert t.blocked == []
        assert t.blocks == []
        assert t.contexts == []
        assert t.due_date == datetime(2020, 1, 2).date()
        assert t.priority == 684
        assert t.projects == []
        assert t.t_date is None
        assert t.task_string == "basic due date duy:01-02"
        assert t.other_string == "basic due date"

    @freeze_time("2020-12-19")
    def test_duy_date_start_december(self):
        """Happy flow of a due date at the start of the year, while being in december."""
        t = Task(1, "basic due date duy:01-02")
        assert t.i == 1
        assert t.blocked == []
        assert t.blocks == []
        assert t.contexts == []
        assert t.due_date == datetime(2021, 1, 2).date()
        assert t.priority == 684
        assert t.projects == []
        assert t.t_date is None
        assert t.task_string == "basic due date duy:01-02"
        assert t.other_string == "basic due date"

    @freeze_time("2020-11-02")
    def test_duy_date_multiple(self):
        """Happy flow of multiple due dates."""
        t = Task(1, "basic due date duy:12-29 duy:12-30")
        assert t.i == 1
        assert t.blocked == []
        assert t.blocks == []
        assert t.contexts == []
        assert t.due_date_all == [datetime(2020, 12, 29).date(), datetime(2020, 12, 30).date()]
        assert t.priority == 684
        assert t.projects == []
        assert t.t_date is None
        assert t.task_string == "basic due date duy:12-29 duy:12-30"
        assert t.other_string == "basic due date"

    def test_duy_date_invalid(self):
        """Flow of an invalid due date. February does not have 31 days."""
        try:
            Task(1, "basic due date duy:02-31")
        except TaskParseError:
            pass

    def test_dum_date_basic(self):
        """Happy flow of a due date."""
        t = Task(1, "basic due date dum:12")
        assert t.i == 1
        assert t.blocked == []
        assert t.blocks == []
        assert t.contexts == []
        assert t.due_date == datetime(self.year, self.month, 12).date()
        assert t.priority == 684
        assert t.projects == []
        assert t.t_date is None
        assert t.task_string == "basic due date dum:12"
        assert t.other_string == "basic due date"

    @freeze_time("2020-01-15")
    def test_dum_date_start(self):
        """Happy flow of a due date in the beginning of the month while today is in the middle."""
        t = Task(1, "basic due date dum:02")
        assert t.i == 1
        assert t.blocked == []
        assert t.blocks == []
        assert t.contexts == []
        assert t.due_date == datetime(2020, 1, 2).date()
        assert t.priority == 684
        assert t.projects == []
        assert t.t_date is None
        assert t.task_string == "basic due date dum:02"
        assert t.other_string == "basic due date"

    @freeze_time("2020-01-01")
    def test_dum_date_start_start(self):
        """Happy flow of a due date in the beginning of the month with a deadline of tomorrow."""
        t = Task(1, "basic due date dum:02 ")
        assert t.i == 1
        assert t.blocked == []
        assert t.blocks == []
        assert t.contexts == []
        assert t.due_date.month == 1
        assert t.priority == 684
        assert t.projects == []
        assert t.t_date is None
        assert t.task_string == "basic due date dum:02"
        assert t.other_string == "basic due date"

    @freeze_time("2020-01-29")
    def test_dum_date_start_next_month(self):
        """Happy flow of a due date in the beginning of the month with a deadline in a few days of the next month."""
        t = Task(1, "basic due date dum:02 ")
        assert t.i == 1
        assert t.blocked == []
        assert t.blocks == []
        assert t.contexts == []
        assert t.due_date.month == 2
        assert t.priority == 684
        assert t.projects == []
        assert t.t_date is None
        assert t.task_string == "basic due date dum:02"
        assert t.other_string == "basic due date"

    def test_dum_date_multiple(self):
        """Happy flow of multiple due dates."""
        t = Task(1, "basic due date dum:08 dum:15")
        assert t.i == 1
        assert t.blocked == []
        assert t.blocks == []
        assert t.contexts == []
        assert t.due_date_all == [datetime(self.year, self.month, 8).date(), datetime(self.year, self.month, 15).date()]
        assert t.priority == 684
        assert t.projects == []
        assert t.t_date is None
        assert t.task_string == "basic due date dum:08 dum:15"
        assert t.other_string == "basic due date"

    def test_dum_date_invalid(self):
        """Flow of an invalid due date."""
        try:
            Task(1, "basic due date dum:32")
            assert False, "Error was not raised."
        except TaskParseError:
            pass

    @freeze_time("2020-12-29")
    def test_dum_date_start_december(self):
        """December test"""
        t = Task(1, "basic due date dum:02")
        assert t.i == 1
        assert t.blocked == []
        assert t.blocks == []
        assert t.contexts == []
        assert t.due_date == datetime(2021, 1, 2).date()
        assert t.priority == 684
        assert t.projects == []
        assert t.t_date is None
        assert t.task_string == "basic due date dum:02"
        assert t.other_string == "basic due date"

