#! /usr/bin/python
# Test Suite for the Task.py
import io
from contextlib import redirect_stdout
from datetime import datetime

import pytest

from todotree.Task.Task import Task
from todotree.Errors.TaskParseError import TaskParseError


class TestTasks:
    """Test the task method."""

    def test_empty(self):
        """Test the settings of an empty task."""
        t = Task(1, "")
        assert t.i == 1
        assert t.blocked == []
        assert t.blocks == []
        assert t.contexts == []
        assert t.due_date is None
        assert t.priority == 684
        assert t.projects == []
        assert t.t_date is None
        assert t.task_string == ""
        assert t.other_string == ""

    def test_basic_task(self):
        """Test a task without any special features."""
        t = Task(1, "basic task")
        assert t.i == 1
        assert t.blocked == []
        assert t.blocks == []
        assert t.contexts == []
        assert t.due_date is None
        assert t.priority == 684
        assert t.projects == []
        assert t.t_date is None
        assert t.task_string == "basic task"
        assert t.other_string == "basic task"

    def test_i_task(self):
        """Test a task with a different index number."""
        t = Task(684, "basic task")
        assert t.i == 684
        assert t.blocked == []
        assert t.blocks == []
        assert t.contexts == []
        assert t.due_date is None
        assert t.priority == 684
        assert t.projects == []
        assert t.t_date is None
        assert t.task_string == "basic task"
        assert t.other_string == "basic task"

    def test_tdate_task(self):
        """Test a task with a tdate."""
        t = Task(1, "test task with tdate in the past t:2020-01-01")
        assert t.i == 1
        assert t.blocked == []
        assert t.blocks == []
        assert t.contexts == []
        assert t.due_date is None
        assert t.priority == 684
        assert t.projects == []
        assert t.t_date == datetime(2020, 1, 1).date()
        assert t.task_string == "test task with tdate in the past t:2020-01-01"
        assert t.other_string == "test task with tdate in the past"

    def test_tdate_multiple_task(self):
        """Test a task with multiple tdates."""
        tsk = "test task with multiple tdates t:2020-01-01 t:2050-12-20"
        t = Task(1, tsk)
        assert t.i == 1
        assert t.blocked == []
        assert t.blocks == []
        assert t.contexts == []
        assert t.due_date is None
        assert t.priority == 684
        assert t.projects == []
        assert t.t_date == datetime(2050, 12, 20).date()
        assert t.task_string == tsk
        assert t.other_string == "test task with multiple tdates"

    def test_tdate_future_task(self):
        """Test a task that has a tdate in the future"""
        tsk = "test task with future tdates t:2500-12-20"
        t = Task(1, tsk)
        assert t.i == 1
        assert t.blocked == []
        assert t.blocks == []
        assert t.contexts == []
        assert t.due_date is None
        assert t.priority == 684
        assert t.projects == []
        assert t.t_date == datetime(2500, 12, 20).date()
        assert t.task_string == tsk
        assert t.other_string == "test task with future tdates"

    def test_tdate_invalid_task(self):
        """Test a task with a tdate which is in the past."""
        tsk = "test task with invalid tdate which should be ignored t:2021-02-29"
        try:
            Task(1, tsk)
            assert False
        except TaskParseError:
            pass

    def test_tdate_invalid_task_msg(self):
        """
        Test a task with an invalid tdate.

        29 February does not exist in 2021, because it is not a leap year.
        """
        tsk = "ignore invalid tdate t:2021-02-29"
        out = io.StringIO()
        with redirect_stdout(out):
            try:
                Task(1, tsk)
            except TaskParseError:
                pass

    def test_append_basic(self):
        """Test when appending to a task."""
        tsk = "basic task"
        t = Task(1, tsk) + "more text"
        assert t.task_string == "basic task more text"
        assert t.i == 1
        assert t.blocked == []
        assert t.blocks == []
        assert t.contexts == []
        assert t.due_date is None
        assert t.priority == 684
        assert t.projects == []
        assert t.t_date is None
        assert t.other_string == "basic task more text"

    def test_append_space(self):
        """Test that the spaces in from are stripped when appending to a task."""
        tsk = "basic task"
        t = Task(1, tsk) + "       more text with spaces in front"
        assert t.task_string == "basic task more text with spaces in front"
        assert t.i == 1
        assert t.blocked == []
        assert t.blocks == []
        assert t.contexts == []
        assert t.due_date is None
        assert t.priority == 684
        assert t.projects == []
        assert t.t_date is None
        assert t.other_string == "basic task more text with spaces in front"

    def test_add(self):
        """Test that the spaces in from are stripped when appending to a task."""
        tsk = "basic task"
        t = Task(1, tsk) + Task(2, tsk)
        assert t.task_string == "basic task basic task"
        assert t.i == 1
        assert t.blocked == []
        assert t.blocks == []
        assert t.contexts == []
        assert t.due_date is None
        assert t.priority == 684
        assert t.projects == []
        assert t.t_date is None
        assert t.other_string == "basic task basic task"

    def test_block(self):
        """Happy flow of a block task."""
        out = io.StringIO()
        with redirect_stdout(out):
            t = Task(1, "basic block bl:basic")
        assert out.getvalue() == ""
        assert t.i == 1
        assert t.blocked == []
        assert t.blocks == ["basic"]
        assert t.contexts == []
        assert t.due_date is None
        assert t.priority == 684
        assert t.projects == []
        assert t.t_date is None
        assert t.task_string == "basic block bl:basic"
        assert t.other_string == "basic block"

    def test_block_multiple(self):
        """Happy flow of multiple block tasks."""
        out = io.StringIO()
        with redirect_stdout(out):
            t = Task(1, "basic block bl:basic bl:more")
        assert out.getvalue() == ""
        assert t.i == 1
        assert t.blocked == []
        assert t.blocks == ["basic", "more"]
        assert t.contexts == []
        assert t.due_date is None
        assert t.priority == 684
        assert t.projects == []
        assert t.t_date is None
        assert t.task_string == "basic block bl:basic bl:more"
        assert t.other_string == "basic block"

    def test_blocked(self):
        """Happy flow of a blocked task."""
        out = io.StringIO()
        with redirect_stdout(out):
            t = Task(1, "basic block by:basic")
        assert out.getvalue() == ""
        assert t.i == 1
        assert t.blocked == ["basic"]
        assert t.blocks == []
        assert t.contexts == []
        assert t.due_date is None
        assert t.priority == 684
        assert t.projects == []
        assert t.t_date is None
        assert t.task_string == "basic block by:basic"
        assert t.other_string == "basic block"

    def test_blocked_multiple(self):
        """Happy flow of multiple blocked tasks."""
        out = io.StringIO()
        with redirect_stdout(out):
            t = Task(1, "basic block by:basic by:more")
        assert out.getvalue() == ""
        assert t.i == 1
        assert t.blocked == ["basic", "more"]
        assert t.blocks == []
        assert t.contexts == []
        assert t.due_date is None
        assert t.priority == 684
        assert t.projects == []
        assert t.t_date is None
        assert t.task_string == "basic block by:basic by:more"
        assert t.other_string == "basic block"

    def test_prio(self):
        """Happy flow of a task with a priority."""
        out = io.StringIO()
        with redirect_stdout(out):
            t = Task(1, "(D) basic priority test")
        assert out.getvalue() == ""
        assert t.i == 1
        assert t.blocked == []
        assert t.blocks == []
        assert t.contexts == []
        assert t.due_date is None
        assert t.priority == 3
        assert t.projects == []
        assert t.t_date is None
        assert t.task_string == "(D) basic priority test"
        assert t.other_string == "basic priority test"

    def test_prio_invalid(self):
        """Flow when parsing an invalid priority."""
        out = io.StringIO()
        with redirect_stdout(out):
            t = Task(1, "(zz) invalid priority")
        assert out.getvalue() == ""
        assert t.i == 1
        assert t.blocked == []
        assert t.blocks == []
        assert t.contexts == []
        assert t.due_date is None
        assert t.priority == 684
        assert t.projects == []
        assert t.t_date is None
        assert t.task_string == "(zz) invalid priority"
        assert t.other_string == "(zz) invalid priority"

    def test_project(self):
        """Happy flow of a project."""
        out = io.StringIO()
        with redirect_stdout(out):
            t = Task(1, "projects +one +two")
        assert out.getvalue() == ""
        assert t.i == 1
        assert t.blocked == []
        assert t.blocks == []
        assert t.contexts == []
        assert t.due_date is None
        assert t.priority == 684
        assert t.projects == ["one", "two"]
        assert t.t_date is None
        assert t.task_string == "projects +one +two"
        assert t.other_string == "projects"

    def test_context(self):
        """Happy flow of a context."""
        out = io.StringIO()
        with redirect_stdout(out):
            t = Task(1, "contexts @one @two")
        assert out.getvalue() == ""
        assert t.i == 1
        assert t.blocked == []
        assert t.blocks == []
        assert t.contexts == ["one", "two"]
        assert t.due_date is None
        assert t.priority == 684
        assert t.projects == []
        assert t.t_date is None
        assert t.task_string == "contexts @one @two"
        assert t.other_string == "contexts"

    def test_all(self):
        """Happy flow when all features are added to a task."""
        out = io.StringIO()
        task_string = "(D) Everything by:basic by:more bl:block bl:more " \
                      "t:2020-02-29 t:2050-05-12 due:2020-02-29 " \
                      "due:2052-02-20 @first @home +todotree +testing"
        with redirect_stdout(out):
            t = Task(427, task_string)
        assert out.getvalue() == ""
        assert t.i == 427
        assert t.blocked == ["basic", "more"]
        assert t.blocks == ["block", "more"]
        assert t.contexts == ["first", "home"]
        assert t.due_date == datetime(2020, 2, 29).date()
        assert t.priority == 3
        assert t.projects == ["todotree", "testing"]
        assert t.t_date == datetime(2050, 5, 12).date()
        assert t.task_string == task_string
        assert t.other_string == "Everything"

    def test_update_priority(self):
        """Update priority of an existing task."""
        # Arrange.
        task = Task(1, "(B) update")
        # Act.
        task.add_or_update_priority("D")
        # Assert.
        assert task.priority == 3
        assert task.task_string == "(D) update"

    def test_update_due(self):
        """Update due date of an existing task."""
        task = Task(1, "test due:2020-02-02")
        task.add_or_update_due(datetime(2020, 2, 10))

        assert task.due_date == datetime(2020, 2, 10).date()
        assert task.task_string == "test due:2020-02-10"

    def test_add_due(self):
        """Add due date of an existing task."""
        task = Task(1, "test")
        task.add_or_update_due(datetime(2020, 2, 10))

        assert task.due_date == datetime(2020, 2, 10).date()
        assert task.task_string == "test due:2020-02-10"

    def test_update_tdate(self):
        """Update tdate of an existing task."""
        task = Task(1, "test t:2020-02-02")
        task.add_or_update_t_date("2020-02-10")

        assert task.t_date == datetime(2020, 2, 10).date()
        assert task.task_string == "test t:2020-02-10"

    def test_add_invalid(self):
        """Add invalid thing to a task."""

        with pytest.raises(TypeError):
            Task(1, "b") + (1, 2)
