from todotree.Task.ConsoleTask import ConsoleTask


class TestConsoleTask:
    """
    Test the console class functionality.
    """

    def test_empty(self):
        ct = ConsoleTask(1, "", 1)
        assert str(ct) == "1"

    def test_default(self):
        ct = ConsoleTask(23, "(D) Everything by:basic by:more bl:block bl:more "
                             "t:2020-02-29 t:2050-05-12 due:2020-02-29 "
                             "due:2052-02-20 @first @home +todotree +testing", 3)
        assert str(
            ct) == "023 (D) Everything due:2020-02-29 +todotree +testing @first @home bl:block bl:more"

    def test_project(self):
        ct = ConsoleTask(23, "(D) Everything by:basic by:more bl:block bl:more "
                             "t:2020-02-29 t:2050-05-12 due:2020-02-29 "
                             "due:2052-02-20 @first @home +todotree +testing", 3)
        assert ct.print_project() == "023 (D) Everything due:2020-02-29 +todotree +testing @first @home bl:block bl:more"

    def test_project_exclude(self):
        ct = ConsoleTask(23, "(D) Everything by:basic by:more bl:block bl:more "
                             "t:2020-02-29 t:2050-05-12 due:2020-02-29 "
                             "due:2052-02-20 @first @home +todotree", 3)
        assert ct.print_project("testing") == "023 (D) Everything due:2020-02-29 +todotree @first @home bl:block bl:more"

    def test_context(self):
        ct = ConsoleTask(23, "(D) Everything by:basic by:more bl:block bl:more "
                             "t:2020-02-29 t:2050-05-12 due:2020-02-29 "
                             "due:2052-02-20 @first @home +todotree +testing", 3)
        assert ct.print_context() == "023 (D) Everything due:2020-02-29 +todotree +testing @first @home bl:block bl:more"

    def test_context_exclude(self):
        ct = ConsoleTask(23, "(D) Everything by:basic by:more bl:block bl:more "
                             "t:2020-02-29 t:2050-05-12 due:2020-02-29 "
                             "due:2052-02-20 @first @home +todotree +testing", 3)
        assert ct.print_context("home") == "023 (D) Everything due:2020-02-29 +todotree +testing @first bl:block bl:more"

    def test_due(self):
        ct = ConsoleTask(23, "(D) Everything by:basic by:more bl:block bl:more "
                             "t:2020-02-29 t:2050-05-12 due:2020-02-29 "
                             "due:2052-02-20 @first @home +todotree +testing", 3)
        assert ct.print_due() == "023 (D) Everything +todotree +testing @first @home bl:block bl:more due:2020-02-29"
