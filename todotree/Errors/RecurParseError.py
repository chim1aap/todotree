from todotree.Errors.TodotreeError import TodotreeError


class RecurParseError(TodotreeError):
    """
    Represents an error when parsing a recur task.
    """

    pass
