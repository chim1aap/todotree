from click.testing import CliRunner

from tests.helper import setup_files
from todotree.Main import root


class TestTaskLifeCycle:
    """Test life cycle of tasks."""

    def test_task_chaining(self, monkeypatch):
        runner = CliRunner()
        with runner.isolated_filesystem():
            # Setup.
            a_num = 65  # chr(65) = 'A'
            m_num = 78  # Some other number.
            recur_content = "".join([f"2020-01-10 ;; daily - ({chr(i)}) added by recur\n" for i in range(a_num, m_num)])
            todotxt, donetxt, *_ = setup_files(monkeypatch, todotxt_content="", donetxt_content="",
                                               recur_content=recur_content)

            # Add tasks via recur.
            result_recur = runner.invoke(root, "recur")
            assert result_recur.exit_code == 0
            assert "1 (A) added by recur" in result_recur.output
            assert "10 (J) added by recur" in result_recur.output
            assert "12 (L) added by recur" in result_recur.output
            assert len(todotxt.read_text().splitlines()) == m_num - a_num

            # Create a new task.
            result_create = runner.invoke(root, "add new task by add")
            assert result_create.exit_code == 0
            assert f"{m_num - a_num + 1} new task by add" in result_create.output
            assert len(todotxt.read_text().splitlines()) == m_num - a_num + 1

            # Append to the task.
            result_append = runner.invoke(root, f"append 12 append to task")
            assert result_append.exit_code == 0
            assert f"(L) added by recur append to task" in result_append.output
            assert len(todotxt.read_text().splitlines()) == m_num - a_num + 1

            # Do the task.
            result_delete = runner.invoke(root, f"do {' '.join([str(i) for i in range(1, m_num + 2 - a_num)])}")
            assert result_delete.exit_code == 0
            assert "(A) added by recur" in result_delete.output
            assert "(L) added by recur append to task" in result_delete.output
            assert len(todotxt.read_text().splitlines()) == 0
            assert todotxt.read_text().strip() == ""
            assert len(donetxt.read_text().splitlines()) == m_num - a_num + 1

    def test_with_empty_tasks(self, monkeypatch):
        runner = CliRunner()
        with runner.isolated_filesystem():
            # Setup.
            todotxt_content = "\n" * 100 + "Task number 101.\n"
            todotxt, donetxt, *_ = setup_files(monkeypatch, todotxt_content=todotxt_content, donetxt_content="")

            # Create a new task.
            result_create = runner.invoke(root, "add new task by add")
            assert result_create.exit_code == 0
            assert f"1 new task by add" in result_create.output
            assert len(todotxt.read_text().splitlines()) == 101

            # Append to the task.
            result_append = runner.invoke(root, f"list")
            assert result_append.exit_code == 0
            assert "101 Task number 101" in result_append.output
            assert "1 new task by add" in result_append.output

            # Do the task.
            result_delete = runner.invoke(root, "do 1 101")
            assert result_delete.exit_code == 0
            assert "Task number 101" in result_delete.output
            assert "new task by add" in result_delete.output
            assert len(todotxt.read_text().splitlines()) == 0
            assert todotxt.read_text().strip() == ""
            assert len(donetxt.read_text().splitlines()) == 2
