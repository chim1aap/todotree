import datetime

from click.testing import CliRunner

from tests.helper import setup_files, setup_git
from todotree.Main import root


class TestRecur:

    today = datetime.date.today().isoformat()

    def test_empty(self, monkeypatch):
        """Test that the command complains when the recur.txt does not exist."""
        # Arrange.
        runner = CliRunner()
        with runner.isolated_filesystem():
            setup_files(monkeypatch)

            # Act.
            result = runner.invoke(root, "recur")

            # Assert.
            print(result.output)
            assert result.exit_code == 1
            assert "recur.txt not found" in result.output

    def test_invalid(self, monkeypatch):
        """Test that the command complains when the recur.txt contains errors."""
        # Arrange.
        runner = CliRunner()

        recur_content = """
                2020-21-10 ;            ; daily - (A) Start date incorrect
                2050-01-31 ; 2022-30-02 ; daily - (B) End date incorrect
                2050-01-31 ;            ; Annually - (C) Interval incorrect
                2020-01-10 ; 2022-01-01 ; daily ; (D) String itself incorrect
                """
        with runner.isolated_filesystem():
            todotxt, *_ = setup_files(monkeypatch, recur_content=recur_content, todotxt_content="")

            # Act.
            result = runner.invoke(root, "recur")

            # Assert.
            todotxt_content = todotxt.read_text()
            print(result.output)
            assert result.exit_code == 1
            assert "recur.txt not found" not in result.output
            assert "Added the following tasks" not in result.output
            assert "Error parsing start date" in result.output
            assert "Error parsing end date" in result.output
            assert "Error parsing interval" in result.output
            assert "This string does not match regex" in result.output
            assert todotxt_content == ""

    def test_add_one(self, monkeypatch):
        """Test that the command adds a recurring task if it does not exist."""
        # Arrange.
        runner = CliRunner()

        recur_content = """
                        2020-01-10 ;            ; daily - (C) added
                        2050-01-10 ;            ; daily - not added
                        2020-01-10 ; 2022-01-01 ; daily - not added
                        """
        with runner.isolated_filesystem():
            todotxt, *_ = setup_files(monkeypatch, recur_content=recur_content, todotxt_content="")

            # Act.
            result = runner.invoke(root, "recur")

            # Assert.
            todotxt_content = todotxt.read_text()
            print(result.output)
            assert result.exit_code == 0
            assert "recur.txt not found" not in result.output
            assert "Added the following tasks" in result.output
            assert "(C) added" in result.output
            assert todotxt_content == "(C) added\n"

    def test_malformed_timestamp_file_whitespace(self, monkeypatch):
        """Test that the command when someone manually edited the timestamp file and left whitespace."""
        # Arrange.
        runner = CliRunner()

        recur_content = """
                        2020-01-10 ;            ; Daily - (C) added
                        """
        with runner.isolated_filesystem():
            todotxt, *_ = setup_files(monkeypatch, recur_content=recur_content, todotxt_content="")
            (todotxt.parent / ".recur.timestamp").write_text("2020-01-12\n")
            # Act.
            result = runner.invoke(root, "recur")

            # Assert.
            todotxt_content = todotxt.read_text()
            print(result.output)
            assert result.exit_code == 0
            assert "recur.txt not found" not in result.output
            assert "Added the following tasks" in result.output
            assert "(C) added" in result.output
            assert todotxt_content == "(C) added\n"

    def test_malformed_timestamp_file(self, monkeypatch):
        """Test that the command fails when the timestamp file is malformed."""
        # Arrange.
        runner = CliRunner()

        recur_content = """
                        2050-01-10 ;            ; Daily - (C) added
                        """
        with runner.isolated_filesystem():
            todotxt, *_ = setup_files(monkeypatch, recur_content=recur_content, todotxt_content="")
            (todotxt.parent / ".recur.timestamp").write_text("malformed\n")
            # Act.
            result = runner.invoke(root, "recur")

            # Assert.
            todotxt_content = todotxt.read_text()
            print(result.output)
            assert result.exit_code == 1
            assert "recur.txt not found" not in result.output
            assert "Added the following tasks" not in result.output
            assert "(C) added" not in result.output
            assert "You can repair this by adding the --date {date} option to recur" in result.output

    def test_override_timestamp_file(self, monkeypatch):
        """Test that the command adds a recurring task even if the timestamp file has a different date."""
        # Arrange.
        runner = CliRunner()

        recur_content = """
                        2020-01-10 ;            ; Daily - (C) added
                        """
        with runner.isolated_filesystem():
            todotxt, *_ = setup_files(monkeypatch, recur_content=recur_content, todotxt_content="")
            (todotxt.parent / ".recur.timestamp").write_text("2021-01-01\n")

            # Act.
            result = runner.invoke(root, "recur --date 2020-01-10")

            # Assert.
            todotxt_content = todotxt.read_text()
            print(result.output)
            assert result.exit_code == 0
            assert "recur.txt not found" not in result.output
            assert "Added the following tasks" in result.output
            assert "(C) added" in result.output
            assert todotxt_content == "(C) added\n"


    def test_override_malformed_timestamp_file(self, monkeypatch):
        """Test that the recur command ignores the timestamp file when overriding."""
        # Arrange.
        runner = CliRunner()

        recur_content = """
                        2020-01-10 ;            ; Daily - (C) added
                        """
        with runner.isolated_filesystem():
            todotxt, *_ = setup_files(monkeypatch, recur_content=recur_content, todotxt_content="")
            (todotxt.parent / ".recur.timestamp").write_text("malformed\n")

            # Act.
            result = runner.invoke(root, "recur --date 2020-01-10")

            # Assert.
            todotxt_content = todotxt.read_text()
            print(result.output)
            assert result.exit_code == 0
            assert "recur.txt not found" not in result.output
            assert "Added the following tasks" in result.output
            assert "(C) added" in result.output
            assert todotxt_content == "(C) added\n"

    def test_add_multiple(self, monkeypatch):
        """Test that when a recurring task has to be added multiple times, it once is added once."""
        # Arrange.
        runner = CliRunner()

        recur_content = """
                2020-01-10 ;            ; daily - (C) added
                2020-01-10 ;            ; daily - (B) added
                2020-01-10 ; 2052-01-01 ; daily - (A) added
                """

        with runner.isolated_filesystem():
            todotxt, *_ = setup_files(monkeypatch, recur_content=recur_content, todotxt_content="")

            # Act.
            result = runner.invoke(root, "recur")

            # Assert.
            todotxt_content = todotxt.read_text()
        print(result.output)
        assert result.exit_code == 0
        assert "recur.txt not found" not in result.output
        assert "Added the following tasks" in result.output
        assert "(C) added" in result.output
        assert "(B) added" in result.output
        assert "(A) added" in result.output
        assert todotxt_content == "(C) added\n(B) added\n(A) added\n"

    def test_skip_existing(self, monkeypatch):
        """Test that existing tasks are not added again"""
        # Arrange.
        runner = CliRunner()
        recur_content = """
                2020-01-10 ;            ; daily - (C) added
                2020-01-10 ;            ; daily - (B) not added
                2020-01-10 ; 2052-01-01 ; daily - (A) added
                """
        with runner.isolated_filesystem():
            todotxt, *_ = setup_files(monkeypatch, recur_content=recur_content, todotxt_content="""(B) not added\n""")

            # Act.
            result = runner.invoke(root, "recur")

            # Assert.
            todotxt_content = todotxt.read_text()
            print(result.output)
            assert result.exit_code == 0
            assert "recur.txt not found" not in result.output
            assert "Added the following tasks" in result.output
            assert "2 (C) added" in result.output
            assert "(B) not added" not in result.output
            assert "3 (A) added" in result.output
            assert todotxt_content == "(B) not added\n(C) added\n(A) added\n"

    def test_not_added(self, monkeypatch):
        """Test that all recurring tasks which do not get added."""
        # Arrange.
        runner = CliRunner()

        recur_content = """
            2090-01-10 ;            ; daily - (C) not added
            2020-01-10 ;            ; daily - (B) not added
            2000-01-10 ; 2002-01-01 ; daily - (A) not added"""

        with runner.isolated_filesystem():
            todotxt, *_ = setup_files(monkeypatch, recur_content=recur_content, todotxt_content="(B) not added\n")

            # Act.
            result = runner.invoke(root, "recur")

            # Assert.
            todotxt_content = todotxt.read_text()
            print(result.output)
            assert result.exit_code == 0
            assert "recur.txt not found" not in result.output
            assert "No recurring task to add" in result.output
            assert "(C) not added" not in result.output
            assert "(B) not added" not in result.output
            assert "(A) not added" not in result.output
            assert todotxt_content == "(B) not added\n"

    def test_run_multiple(self, monkeypatch):
        """Test that the command adds nothing if run twice."""
        # Arrange.
        runner = CliRunner()

        today = datetime.date.today()
        recur_content = f"""
                        2020-01-01 ;; daily - (D) added
                        2020-01-01 ;; weekly - (W) added
                        2020-01-01 ;; monthly - (M) added
                        2020-01-01 ;; yearly - (Y) added
                        {today} ;; never - (N) added
                        """
        config_content = """git:
            mode: full"""
        with runner.isolated_filesystem() as remote:
            with runner.isolated_filesystem():

                todotxt, *_ = setup_files(monkeypatch, recur_content=recur_content, todotxt_content="", configyaml_content=config_content)
                setup_git(remote, todotxt.parent)

                # Act.
                result = runner.invoke(root, "recur")
                result_nothing = runner.invoke(root, "recur")

                # Assert.
                print(result.output)
                print(result_nothing.output)
                todotxt_content = todotxt.read_text()
                timestamp_file = (todotxt.parent / ".recur.timestamp").read_text()

                assert result.exit_code == 0
                assert "recur.txt not found" not in result.output
                assert "Added the following tasks" in result.output
                assert "Added the following tasks" not in result_nothing.output
                assert "Push successful" in result.output
                assert "Push successful" not in result_nothing.output
                assert "(Y) added" in result.output
                assert "Recur has already run today" in result_nothing.output
                assert self.today in timestamp_file
                assert todotxt_content == "(D) added\n(W) added\n(M) added\n(Y) added\n"

    def test_run_next_day(self, monkeypatch):
        """Test when running it on a cron job, ie, the next run is after Exactly 24 hours, rather and sometime next day."""
        # Arrange.
        runner = CliRunner()

        today = datetime.date.today()
        recur_content = f"""
                        2020-01-01 ;; daily - (D) added
                        2020-01-01 ;; weekly - (W) added
                        2020-01-01 ;; monthly - (M) added
                        2020-01-01 ;; yearly - (Y) added
                        {today} ;; never - (N) added
                        """
        config_content = """git:
            mode: full"""
        with runner.isolated_filesystem() as remote:
            with runner.isolated_filesystem():

                todotxt, *_ = setup_files(monkeypatch, recur_content=recur_content, todotxt_content="", configyaml_content=config_content)
                setup_git(remote, todotxt.parent)

                # Act.
                result = runner.invoke(root, "recur --verbose")
                timestamp_file = (todotxt.parent / ".recur.timestamp")
                timestamp_old = datetime.date.today()
                timestamp_new = timestamp_old - datetime.timedelta(days=1)
                timestamp_file.write_text(timestamp_new.isoformat())
                result_the_day_after = runner.invoke(root, "recur --verbose")

                # Assert.
                print(result.output)
                print(result_the_day_after.output)
                todotxt_content = todotxt.read_text()
                assert result.exit_code == 0
                assert "recur.txt not found" not in result.output
                assert "Added the following tasks" in result.output
                assert "No recurring task to add." in result_the_day_after.output
