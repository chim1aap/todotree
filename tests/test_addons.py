import shutil
from pathlib import Path

from click.testing import CliRunner

from tests.helper import setup_files
from todotree.Main import root


class TestAddons:
    """Test the add-on functionality."""

    def test_help(self):
        """Test that the help shows the addon names."""
        # Arrange.
        runner = CliRunner()

        # Act.
        result = runner.invoke(root, "addons --help")

        assert result.exit_code == 0

    def test_run(self, monkeypatch):
        """Test that an addon can be run"""
        # Arrange.
        runner = CliRunner()
        addons_path = Path("./addons/hello.py").resolve()
        with runner.isolated_filesystem():
            todotxt, *_ = setup_files(monkeypatch, todotxt_content="")
            (todotxt.parent / "addons").mkdir()
            shutil.copy(addons_path, (todotxt.parent / "addons" / "hello.py"))

            # Act.
            result = runner.invoke(root, "addons hello.py")

        print(result.output)
        assert result.exit_code == 0
        assert "Hello World!\n" in result.output

    def test_poem(self, monkeypatch):
        """Test that the poem addon also works."""
        # Arrange.
        runner = CliRunner()
        addons_path = Path("./addons/poem.py").resolve()
        with runner.isolated_filesystem():
            todotxt, *_ = setup_files(monkeypatch, todotxt_content="")
            (todotxt.parent / "addons").mkdir()
            shutil.copy(addons_path, (todotxt.parent / "addons" / "poem.py"))

            # Act.
            result = runner.invoke(root, "addons poem.py")

        print(result.output)
        assert result.exit_code == 0
        assert "pip install poetry" in result.output

    def test_invalid(self, monkeypatch):
        """Test that an invalid command still errors."""
        # Arrange.
        runner = CliRunner()

        # Act.
        with runner.isolated_filesystem():
            setup_files(monkeypatch)
            result = runner.invoke(root, "addons does_not_exist")
        assert result.exit_code == 1
        assert result.output.startswith('!!!There is no script at ')

    def test_run_empty(self):
        """Test that addons command with --help will work when there is no addon."""
        # Arrange.
        runner = CliRunner()

        # Act.
        with runner.isolated_filesystem():
            result = runner.invoke(root, "--help addons does_not_exist")
        assert result.exit_code == 0

    def test_run_args_to_addon(self, monkeypatch):
        """Test that arguments after the addon are passed to the addon itself."""
        # Arrange.
        runner = CliRunner()
        addons_path = Path("./addons/hello.py").resolve()
        with runner.isolated_filesystem():
            todotxt, *_ = setup_files(monkeypatch, todotxt_content="")
            (todotxt.parent / "addons").mkdir()
            shutil.copy(addons_path, (todotxt.parent / "addons" / "hello.py"))
            # Act.
            result = runner.invoke(root, "addons hello.py --more")
        print(result.output)
        assert result.exit_code == 0
        assert result.output == "More Hello World!\n"
