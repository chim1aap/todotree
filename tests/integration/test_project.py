from pathlib import Path

import freezegun
from click.testing import CliRunner

from tests.helper import setup_files
from todotree.Errors.TodoFileNotFoundError import TodoFileNotFoundError
from todotree.Main import root
from todotree.Managers.AbstractManager import AbstractManager


class TestProject:
    """Integration tests for project command."""

    def test_project_tree(self, monkeypatch):
        """Test the project tree command."""
        # Arrange.
        runner = CliRunner()

        # Act.
        with runner.isolated_filesystem():
            setup_files(monkeypatch, todotxt_content="First task\nSecond task\nThird task\n")
            result = runner.invoke(root, "project")

        # Assert.
        assert result.exit_code == 0
        assert result.output == """Projects
 *--No Project
     +--1 First task
     +--2 Second task
     *--3 Third task

"""


    def test_no_file_found_project(self, monkeypatch):
        """
        Test that an error is returned when files are not found using `context`.
        """
        # Arrange.
        def mock_open(*args, **kwargs):
            raise TodoFileNotFoundError("Not Found")
        monkeypatch.setattr(AbstractManager, "import_tasks", mock_open)
        runner_todo = CliRunner()
        with runner_todo.isolated_filesystem():
            setup_files(monkeypatch)
            # Act.
            result_todo = runner_todo.invoke(root, "project")

        # Assert
        assert result_todo.exit_code == 1
        assert result_todo.output[0:58] == "!!!The todo.txt could not be found.\n!!!It searched at the "

    def test_only_projects_folder(self, monkeypatch):
        """Test the behaviour when todo.txt is empty and project_folder is enabled."""
        # Arrange.
        runner = CliRunner()
        done_content = "x 2020-01-01 Done task\n"
        with runner.isolated_filesystem():
            config_content = f"""
main:
    enable_project_folder: true
paths:
    project_folder: {(Path.cwd() / "project").absolute()}
            """
            setup_files(monkeypatch, donetxt_content=done_content, configyaml_content=config_content)

            # Create some projects
            cwd = Path.cwd()
            (cwd / "project" / "projectA").mkdir(parents=True, exist_ok=True)
            (cwd / "project" / "projectB").mkdir(parents=True, exist_ok=True)
            (cwd / "project" / "projectC").mkdir(parents=True, exist_ok=True)
            (cwd / "xdg" / "data" / "todotree" / "todo.txt").touch()
            result = runner.invoke(root, "project")
        print(result.output)
        assert result.exit_code == 0
        assert "+projectA" not in result.output
        assert "+projectB" not in result.output
        assert "+projectC" not in result.output
        assert "No Project" not in result.output
