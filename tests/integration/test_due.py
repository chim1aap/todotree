from pathlib import Path

import click
import freezegun
from click.testing import CliRunner
from todotree.Managers.AbstractManager import AbstractManager

from todotree.Errors.TodoFileNotFoundError import TodoFileNotFoundError

from tests.helper import setup_files
from todotree.Main import root


class TestDue:
    """Integration tests for the due command."""

    def test_due(self, monkeypatch):
        """Test the due command"""
        # Arrange.
        runner = CliRunner()

        with runner.isolated_filesystem():
            setup_files(monkeypatch, todotxt_content="First task\nSecond task\nThird task\n")
            # Act.
            result = runner.invoke(root, "due")

        # Assert.
        assert result.exit_code == 0
        assert result.output == """Due Dates
 *--No due date
     +--1 First task
     +--2 Second task
     *--3 Third task

"""

    @freezegun.freeze_time("2023-08-15")
    def test_due_full(self, monkeypatch):
        """Test the due command"""
        # Arrange.
        runner = CliRunner()
        example_todo = Path("../todotree/examples/todo.txt").read_text()
        with runner.isolated_filesystem():
            setup_files(monkeypatch, todotxt_content=example_todo)
            # Act.
            result = runner.invoke(root, "due")

        # Assert.
        assert result.exit_code == 0
        assert result.output == """Due Dates
 +--Overdue
 |   +--02 (D) Example once in a lifetime due date due:2021-02-20
 |   +--03 (D) Example once in a year due date due:2023-02-20
 |   *--04 (D) Example once in a month due date due:2023-08-02
 *--No due date
     +--01 (A) This is an example todo.txt file
     +--07 (B) Learn about all todotree's actions
     +--08 (C) Migrate all todo's to todotree
     +--05 (D) Rewrite regex such that all three above can be done with just "due" +todotree
     +--06 (E) A project is denoted with a plus, +example
     +--09 (E) A context is denoted with an ampersand: @hometest
     +--10 (F) A task which is not shown until Christmas 2020
     +--11 (G) A task which blocks another task bl:a
     +--15 (T) Test: block and tdate. This task should be hidden because block +thind
     +--22 (T) Test: because this task. +nothidden
     *--25 (T) Test: because this task. @nothidden

"""

    def test_no_file_found_due(self, monkeypatch):
        """
        Test that an error is returned when files are not found using `due`.
        """

        # Arrange.
        def mock_open(*args, **kwargs):
            raise TodoFileNotFoundError("Not Found")

        monkeypatch.setattr(AbstractManager, "import_tasks", mock_open)
        runner_todo = CliRunner()
        with runner_todo.isolated_filesystem():
            setup_files(monkeypatch)
            # Act.
            result_todo = runner_todo.invoke(root, "due")

        # Assert
        assert result_todo.exit_code == 1
        assert result_todo.output[0:58] == "!!!The todo.txt could not be found.\n!!!It searched at the "
