import datetime
from pathlib import Path
from unittest import mock

import freezegun

from todotree.Config.Config import Config
from todotree.Managers.AbstractManager import AbstractManager
from todotree.Managers.RecurManager import RecurManager
from todotree.Task.RecurTask import RecurTask
from todotree.Task.Task import Task
from todotree.Managers.TaskManager import TaskManager


class TestRecurManager:

    def test_import_single(self, monkeypatch):
        """Test that it can be imported."""
        recur_content = """
        2020-01-01 ; 2030-01-02 ; daily - add task
        """
        monkeypatch.setattr(Path, "open", mock.mock_open(read_data=recur_content))

        def mock_import(self):
            pass

        monkeypatch.setattr(AbstractManager, "import_tasks", mock_import)

        # Act.
        result = RecurManager(Config())
        result.import_tasks()

        # Assert.
        assert len(result.task_list) == 1
        recur_task: RecurTask = result.task_list[0]
        assert recur_task.task_string == "add task"
        assert recur_task.interval == RecurTask.Recurrence.Daily
        assert recur_task.start_date == datetime.date(2020, 1, 1)
        assert recur_task.end_date == datetime.date(2030, 1, 2)

    def test_import_multiple(self, monkeypatch):
        """Test that multiple can be imported."""
        recur_content = """
        2020-01-01 ; 2030-01-02 ; daily - add task
        # A comment, which is ignored.
        2020-01-01 ;; never - optional task
        """
        monkeypatch.setattr(Path, "open", mock.mock_open(read_data=recur_content))

        def mock_import(self):
            pass

        monkeypatch.setattr(AbstractManager, "import_tasks", mock_import)
        # Act.
        result = RecurManager(Config())
        result.import_tasks()

        # Assert.
        assert len(result.task_list) == 2
        recur_task: RecurTask = result.task_list[0]
        assert recur_task.task_string == "add task"
        assert recur_task.interval == RecurTask.Recurrence.Daily
        assert recur_task.start_date == datetime.date(2020, 1, 1)
        assert recur_task.end_date == datetime.date(2030, 1, 2)

    def test_import_fail(self, monkeypatch):
        """Test that an invalid line fails the import."""
        recur_content = """
        2020-01-01 ; 2030-01-02 ; daily - add task
        # A comment, which is ignored.
        2020-01-01 ;; never - optional task
        """
        monkeypatch.setattr(Path, "open", mock.mock_open(read_data=recur_content))

        def mock_import(self):
            pass

        monkeypatch.setattr(AbstractManager, "import_tasks", mock_import)
        # Act.
        result = RecurManager(Config())
        result.import_tasks()

        # Assert.
        assert len(result.task_list) == 2
        recur_task: RecurTask = result.task_list[0]
        assert recur_task.task_string == "add task"
        assert recur_task.interval == RecurTask.Recurrence.Daily
        assert recur_task.start_date == datetime.date(2020, 1, 1)
        assert recur_task.end_date == datetime.date(2030, 1, 2)

    def test_add_to_todo(self, monkeypatch):
        """Test that recur tasks are added to the todo.txt"""
        recur_content = """
        2020-01-01 ; 2050-01-02 ; daily - existing task in todotxt
        2020-01-01 ; 2050-01-02 ; daily - task which gets added.
        # A comment, which is ignored.
        2020-01-01 ;; never - optional task which does not get added. 
        2020-01-01 ; 2022-01-01 ; daily - Ended task.
        """
        monkeypatch.setattr(Path, "open", mock.mock_open(read_data=recur_content))

        def mock_import(self):
            pass

        monkeypatch.setattr(AbstractManager, "import_tasks", mock_import)

        def mock_import(self):
            self.task_list = [
                Task(1, "existing task in todotxt"),
                Task(2, "other task in todotxt"),
            ]

        added_tasks = []

        def mock_write(self, tasks):
            nonlocal added_tasks
            added_tasks = tasks
            return 1, tasks

        monkeypatch.setattr(TaskManager, "import_tasks", mock_import)
        monkeypatch.setattr(TaskManager, "add_tasks_to_file", mock_write)

        rm = RecurManager(Config())
        rm.import_tasks()
        rm.last_date = datetime.date(2022, 2, 2)
        # Act.
        rm.add_to_todo()

        # Assert.
        assert len(added_tasks) == 1
        assert added_tasks[0].to_file() == "task which gets added.\n"

    @freezegun.freeze_time("2023-06-23")
    def test_add_to_todo_weekly(self, monkeypatch):
        """Test that recur tasks are added to the todo.txt"""
        mdate_file = datetime.date(2023, 6, 21)
        recur_content = """
        2020-06-15 ;; weekly - not added weekly (Nearest date is 2023-06-26)
        2020-06-18 ;; weekly - added weekly (Nearest date is 2023-06-22)
        """
        monkeypatch.setattr(Path, "open", mock.mock_open(read_data=recur_content))

        def mock_import(self):
            pass

        monkeypatch.setattr(AbstractManager, "import_tasks", mock_import)

        def mock_import(self):
            self.task_list = [
                Task(1, "existing task in todotxt"),
                Task(2, "other task in todotxt"),
            ]

        added_tasks = []

        def mock_write(self, tasks):
            nonlocal added_tasks
            added_tasks = tasks
            return 1, tasks

        monkeypatch.setattr(TaskManager, "import_tasks", mock_import)
        monkeypatch.setattr(TaskManager, "add_tasks_to_file", mock_write)

        rm = RecurManager(Config())
        rm.import_tasks()
        rm.last_date = mdate_file
        # Act.
        rm.add_to_todo()

        # Assert.
        assert len(added_tasks) == 1
        assert added_tasks[0].to_file() == "added weekly (Nearest date is 2023-06-22)\n"

    @freezegun.freeze_time("2023-06-23")
    def test_add_to_todo_monthly(self, monkeypatch):
        """Test that recur tasks are added to the todo.txt"""
        mdate_file = datetime.date(2023, 6, 17)
        recur_content = """
        2020-06-15 ;; MoNtHly - not added 
        2020-06-18 ;; MONTHLY - added 
        """
        monkeypatch.setattr(Path, "open", mock.mock_open(read_data=recur_content))

        def mock_import(self):
            pass

        monkeypatch.setattr(AbstractManager, "import_tasks", mock_import)

        def mock_import(self):
            self.task_list = [
                Task(1, "existing task in todotxt"),
                Task(2, "other task in todotxt"),
            ]

        added_tasks = []

        def mock_write(self, tasks):
            nonlocal added_tasks
            added_tasks = tasks
            return 1, tasks

        monkeypatch.setattr(TaskManager, "import_tasks", mock_import)
        monkeypatch.setattr(TaskManager, "add_tasks_to_file", mock_write)

        rm = RecurManager(Config())
        rm.import_tasks()
        rm.last_date = mdate_file
        # Act.
        rm.add_to_todo()

        # Assert.
        assert len(added_tasks) == 1
        assert added_tasks[0].to_file() == "added\n"

    @freezegun.freeze_time("2024-01-05")
    def test_add_to_todo_monthly_edge(self, monkeypatch):
        """Test that recur tasks are added to the todo.txt"""
        mdate_file = datetime.date(2023, 12, 17)
        recur_content = """
        2020-06-15 ;; MoNtHly - not added 
        2020-06-18 ;; MONTHLY - added 
        """
        monkeypatch.setattr(Path, "open", mock.mock_open(read_data=recur_content))

        def mock_import(self):
            pass

        monkeypatch.setattr(AbstractManager, "import_tasks", mock_import)

        def mock_import(self):
            self.task_list = [
                Task(1, "existing task in todotxt"),
                Task(2, "other task in todotxt"),
            ]

        added_tasks = []

        def mock_write(self, tasks):
            nonlocal added_tasks
            added_tasks = tasks
            return 1, tasks

        monkeypatch.setattr(TaskManager, "import_tasks", mock_import)
        monkeypatch.setattr(TaskManager, "add_tasks_to_file", mock_write)

        rm = RecurManager(Config())
        rm.import_tasks()
        rm.last_date = mdate_file
        # Act.
        rm.add_to_todo()

        # Assert.
        assert len(added_tasks) == 1
        assert added_tasks[0].to_file() == "added\n"
